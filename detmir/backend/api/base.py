from fastapi import APIRouter
from api.v1 import route_brands
from api.v1 import route_items

api_router = APIRouter()
api_router.include_router(route_brands.router, prefix="", tags=["brands"])
api_router.include_router(route_items.router, prefix="", tags=["products"])

