from fastapi import Request
from fastapi import APIRouter
from fastapi import Depends
from fastapi.responses import ORJSONResponse
from detmir.api import get_brands
from utils.http_request import get_client


router = APIRouter(include_in_schema=False)


@router.get("/v1/brands/", response_class=ORJSONResponse)
async def brands(request: Request, client = Depends(get_client)):
        output = await get_brands(client)
        return output

