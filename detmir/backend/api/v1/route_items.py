from fastapi import Request
from fastapi import APIRouter
from fastapi import Depends
from fastapi.responses import ORJSONResponse
from detmir.api import get_qty_items
from utils.http_request import get_client
from utils.http_request import get_pool_params
from utils.http_request import pool_requests
from utils.parser import get_product_data

from typing import NamedTuple

class FormData(NamedTuple):
    brandID: str
    region: str


router = APIRouter(include_in_schema=False)


@router.post("/v1/items/", response_class=ORJSONResponse)
async def brands(request: Request, client = Depends(get_client)):
    url = 'https://api.detmir.ru/v2/products'
    data = await request.json()
    postData = FormData(**data)
    qty_items = await get_qty_items(client, postData)
    pool_params = await get_pool_params(postData, qty_items, 30)
    responses = await pool_requests(client, url, pool_params)
    product_data = await get_product_data(responses, postData)
    return product_data



