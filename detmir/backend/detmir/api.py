import httpx
from typing import NamedTuple, Iterable
from typing import Literal
from utils.http_request import request


class Brand(dict):
    brand_id: str
    brand_title: str


async def get_brands(client: httpx.AsyncClient) -> Iterable:
    url = 'https://api.detmir.ru/v2/brands'
    params = {'filter': 'popular:false;site:detmir;type:VENDOR'}
    response = await request(client, url, params)
    brands = response.json()
    output = tuple(Brand(brand_id=brand.get('id'), brand_title=brand.get('title')) for brand in brands)
    return output


async def get_qty_items(client: httpx.AsyncClient, postData) -> int:
    url = 'https://api.detmir.ru/v2/products'
    params = {
        'filter': f'brands[].id:{postData.brandID};promo:false;withregion:{postData.region}',
        'expand': 'meta.facet.ages.adults,meta.facet.gender.adults',
        'exclude': 'brands',
        'meta': '*',
        'limit': 30,
        'offset': 30,
        'sort': 'popularity:desc',
    }

    response = await request(client, url, params)
    output = response.json().get('meta').get('length')
    return output








