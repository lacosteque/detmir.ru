var loader = document.getElementById("loader");

$(function () {
    (async () => {
        try {
            var form = document.getElementById("data");
            loader.classList.remove("d-none");

            let response = await fetch("/v1/brands/");
            if (response.ok) {
                let data = await response.json();
                let html = "";
                for (const key in data) {
                    const value = data[key]["brand_id"];
                    const name = data[key]["brand_title"];
                    html += '<option class="brand" value="' + value + '">' + name + "</option>";
                }

                document.querySelector("#brands").innerHTML = html;
            }
        } catch (error) {
            console.log(error);
        }

        loader.classList.add("d-none");
        form.classList.remove("d-none");

        $(".multiselect").multiselect({
            enableFiltering: true,
            includeFilterClearBtn: false,
            includeSelectAllOption: true,
            maxHeight: 350,
            numberDisplayed: 1,
        });
    })();

    $("#scrape").click(function (e) {
        e.preventDefault();

        loader.classList.remove("d-none");

        var result = document.getElementById("result");
        result.classList.add("d-none");

        if ($(".multiselect-container .active").length > 0) {
            var $table = $("#table");

            var brandID = $("button.multiselect-option.dropdown-item.brand.active input").val();
            var region = $("button.multiselect-option.dropdown-item.region.active input").val();

            var data = { brandID: brandID, region: region };

            var json = JSON.stringify(data);

            (async () => {
                try {
                    let response = await fetch("/v1/items/", {
                        method: "POST",
                        body: json,
                    });

                    if (response.ok) {
                        let data = await response.json();
                        $table.bootstrapTable({ data: data, exportDataType: $(this).val(), exportTypes: ["json", "xml", "csv", "sql", "excel", "xlsx"] }).bootstrapTable("load", data);
                        result.classList.remove("d-none");
                        $("html, body").animate({ scrollTop: $("#result").offset().top }, 1000);
                    }
                } catch (error) {
                    console.log(error);
                }

                loader.classList.add("d-none");
            })();
        }
    });
});
