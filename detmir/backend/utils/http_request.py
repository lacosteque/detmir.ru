import asyncio
import httpx


headers = {
    'authority': 'api.detmir.ru',
    'accept': '*/*',
    'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'cache-control': 'no-cache',
    'content-type': 'application/json',
    'origin': 'https://www.detmir.ru',
    'pragma': 'no-cache',
    'referer': 'https://www.detmir.ru/',
    'sec-ch-ua': '".Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"',
    'sec-ch-ua-mobile': '?1',
    'sec-ch-ua-platform': '"Android"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-site',
    'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Mobile Safari/537.36',
    'x-requested-with': 'detmir-ui',
}


async def get_client():
    limits = httpx.Limits(max_keepalive_connections=5, max_connections=10)
    async with httpx.AsyncClient(limits=limits, timeout=120, headers=headers) as client:
        yield client



async def request(client: httpx.AsyncClient, url: str, params: dict) -> httpx.Response:
    response = await client.get(url, params=params)
    try:
        response.raise_for_status()
        return response
    except httpx.HTTPStatusError as exc:
        print(f"Error response {exc.response.status_code} while requesting {exc.request.url!r}.")



async def pool_requests(client: httpx.AsyncClient, url: str,  pool_params: list):
    tasks = (asyncio.ensure_future(request(client, url, params)) for params in pool_params)
    output = await asyncio.gather(*tasks)
    return output



async def get_pool_params(postData, qty_items: int, limit: int) -> list:
    qty_pages = int(qty_items/limit)
    pool_params = []
    for i in range(qty_pages + 1):
        template = {
            'filter': f'brands[].id:{postData.brandID};promo:false;withregion:{postData.region}',
            'expand': 'meta.facet.ages.adults,meta.facet.gender.adults',
            'exclude': 'brands',
            'meta': '*',
            'limit': limit,
            'offset': int(f'{limit}') * i,
            'sort': 'popularity:desc',
}

        pool_params.append(template)
    return pool_params



