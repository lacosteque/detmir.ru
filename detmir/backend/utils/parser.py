from typing import Iterable

class ProductData(dict):
    product_id: int
    product_title: str
    product_price: int|float
    product_promo_price: int 
    product_url: str
    product_region: str
    available: bool


async def get_product_data(responses: list, postData) -> Iterable:
    product_data = []
    pool_items = (items for response in responses for items in response.json().get('items'))

    for item in pool_items:
        available = False
        product_id = item.get('productId')
        product_title = item.get('title')
        product_url = item.get('link').get('web_url') 
        promo = item.get('promo')
        
        if promo:
            product_promo_price = item.get('price').get('price')
            product_price = item.get('old_price').get('price')
        else:
            product_price = item.get('price').get('price')
            product_promo_price = None

        if postData.region in item.get('available').get('offline').get('region_iso_codes'):
            available = True



        product_data.append(ProductData(product_id=product_id, 
                                        product_title=product_title, 
                                        product_price=product_price, 
                                        product_promo_price=product_promo_price, 
                                        product_url=product_url, 
                                        region=postData.region,
                                        available=available
                                        ))
    return product_data
