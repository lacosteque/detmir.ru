import asyncio
import httpx
from typing import NamedTuple

class ResponseAPI(NamedTuple):
    meta: dict
    items: list

headers = {
    'authority': 'api.detmir.ru',
    'accept': '*/*',
    'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'cache-control': 'no-cache',
    'content-type': 'application/json',
    'origin': 'https://www.detmir.ru',
    'pragma': 'no-cache',
    'referer': 'https://www.detmir.ru/',
    'sec-ch-ua': '".Not/A)Brand";v="99", "Google Chrome";v="103", "Chromium";v="103"',
    'sec-ch-ua-mobile': '?1',
    'sec-ch-ua-platform': '"Android"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-site',
    'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Mobile Safari/537.36',
    'x-requested-with': 'detmir-ui',
}

async def request(client: httpx.AsyncClient, params: dict) -> ResponseAPI:
    url = 'https://api.detmir.ru/v2/products'
    response = await client.get(url, params=params, timeout=30)
    try:
        response.raise_for_status()
        meta = response.json().get('meta')
        items = response.json().get('items')
        return ResponseAPI(meta, items)
    except httpx.HTTPStatusError as exc:
        print(f"Error response {exc.response.status_code} while requesting {exc.request.url!r}.")


async def pool_requests(client: httpx.AsyncClient, pool_params: list):
    tasks = (asyncio.ensure_future(request(client, params)) for params in pool_params)
    output = await asyncio.gather(*tasks)
    return output
