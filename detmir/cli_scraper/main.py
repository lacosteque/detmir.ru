import asyncio
import time
from http_request import pool_requests, headers
from utils import *


async def main():
    url, region = await info()
    t_start = time.monotonic()
    alias = await get_category_alias(url)
    limits = httpx.Limits(max_keepalive_connections=5, max_connections=10)
    async with httpx.AsyncClient(limits=limits, headers=headers) as client:
        qty_items = await get_qty_items(client, alias, region)
        pool_params = await get_pool_params(alias, region, qty_items, 100)
        responses = await pool_requests(client, pool_params)
    product_data = await get_product_data(responses, region)
    await export_csv(product_data, alias, region)
    t_stop = time.monotonic()
    t_run = t_stop - t_start
    print(f'\n\033[32m-----\033[0m Время выполнения - {t_run} сек.')


if __name__ == "__main__":
    asyncio.run(main())
