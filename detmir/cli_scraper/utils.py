import re
import csv
from typing import NamedTuple, Iterable
from datetime import datetime
import httpx
from http_request import request

class ProductData(NamedTuple):
    product_id: int
    product_title: str
    product_price: int|float
    product_promo_price: int 
    product_url: str
    product_region: str
    available: bool


async def get_category_alias(url: str) -> str:
    try:
        pattern = 'https://www\.detmir\.ru/catalog/index/name/([A-Za-z0-9_]+)'
        result = re.search(pattern, url)
        output = result.group(1)
        return output
    except AttributeError:
        print('Ссылка не подходит под шаблон!')
        quit(1)


async def get_pool_params(alias: str, region: str, qty_items: int, limit: int) -> list:
    qty_pages = int(qty_items/limit)
    pool_params = []
    for i in range(qty_pages + 1):
        template = {
            'filter': f'categories[].alias:{alias};promo:false;withregion:{region}',
            'expand': 'meta.facet.ages.adults,meta.facet.gender.adults,webp',
            'meta': '*',
            'limit': limit,
            'offset': int(f'{limit}') * i,
            'sort': 'popularity:desc',
            }
        pool_params.append(template)
    return pool_params


async def get_qty_items(client: httpx.AsyncClient, alias: str, region: str) -> int:
    params = {
            'filter': f'categories[].alias:{alias};promo:false;withregion:{region}',
            'expand': 'meta.facet.ages.adults,meta.facet.gender.adults,webp',
            'meta': '*',
            'limit': 30,
            'offset': 30,
            'sort': 'popularity:desc',
            }
    response = await request(client, params)
    output = response.meta.get('length')
    return output


async def get_product_data(responses: list, region: str) -> Iterable:
    product_data = []
    pool_items = (items for response in responses for items in response.items)

    for item in pool_items:
        available = False
        product_id = item.get('productId')
        product_title = item.get('title')
        product_url = item.get('link').get('web_url') 
        promo = item.get('promo')
        
        if promo:
            product_promo_price = item.get('price').get('price')
            product_price = item.get('old_price').get('price')
        else:
            product_price = item.get('price').get('price')
            product_promo_price = None

        if region in item.get('available').get('offline').get('region_iso_codes'):
            available = True

        product_data.append(ProductData(product_id, 
                                        product_title, 
                                        product_price, 
                                        product_promo_price, 
                                        product_url, 
                                        region,
                                        available
                                        ))
    return product_data

    
async def export_csv(product_data: list, alias: str, region: str) -> None:
    dt = datetime.now()
    timestamp = int(datetime.timestamp(dt))
    current_date = dt.date()

    with open(f'{alias}-{region}-{current_date}-{timestamp}.csv', 'w', newline='') as csvfile:
        fieldnames = ['id', 'title', 'price', 'promo_price', 'url', 'region', 'available']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        
        for product in product_data:
            writer.writerow({'id': product.product_id,
                             'title': product.product_title, 
                             'price': product.product_price, 
                             'promo_price': product.product_promo_price, 
                             'url': product.product_url, 
                             'region': product.product_region,
                             'available': product.available
                             })


async def info():
    print('\033[32m-----\033[0m Введите ссылку на категорию (прим. - https://www.detmir.ru/catalog/index/name/lego/)')
    url = input('[url] - ' )
    
    print('\n\033[32m-----\033[0m Укажите регион (1 - Москва и МО, 2 - Санкт-Петербург и ЛО)')
    regional_selection = input('[region] - ' )
    match regional_selection:
        case '1':
            region = 'RU-MOW'
        case '2':
            region = 'RU-SPE'
        case _:
            print('Региона нет в списке')
            quit(1)
    return url, region
